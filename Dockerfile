FROM docker:20.10.12-alpine3.14@sha256:7576c1354d56ada47f1e52b9f5b9f472cb4bc85e299432b5508b5661228d3104 AS docker

FROM docker/buildx-bin:0.10.4@sha256:0e173164812f853a563cf4d07ef24c3f8a82fd715f665ec50468da59d4f7e437 AS buildx

FROM python:3.9.7-alpine3.14@sha256:a31add3a2442bceecb9d4f66e575072c4d4a1c7e561b7fe44cdf0dd277685276

COPY poetry.lock pyproject.toml /

ARG poetry_version=1.1.10
ARG PATH="/root/.poetry/bin:${PATH}"
COPY --from=docker /usr/local/bin/docker /usr/local/bin/docker
COPY --from=buildx /buildx /usr/local/lib/docker/cli-plugins/docker-buildx
RUN apk add --no-cache \
    git \
 && apk add --no-cache --virtual build-deps \
    cargo \
    gcc \
    libffi-dev \
    make \
    musl-dev \
    openssl-dev \
 && wget "https://raw.githubusercontent.com/python-poetry/poetry/${poetry_version}/get-poetry.py" \
 && python get-poetry.py --version "${poetry_version}" \
 && rm get-poetry.py \
 && poetry config virtualenvs.create false \
 && poetry install --no-root \
 && rm -rf /root/.poetry \
 && apk del --no-cache build-deps
